﻿using Assets.IdleEngine.Sessions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.IdleEngine.Generator
{
    /**
     * https://www.youtube.com/watch?v=ZiugwSysFVY
     * 12:45
     */
    [CreateAssetMenu(fileName = "IdleGenerator", menuName = "Game/IdleGenerator", order = 0)]
    public class IdleGenerator : ScriptableObject
    {
        public int Owned;
        public double BaseCost;
        public double BaseRevenue;
        public float ProductionTimeInSeconds;
        public double CostFactor;

        public bool CanBeBuild(Session session)
        {

        }

        public void Build(Session session)
        {

        }

        public void Produce(float depletedTimeInSeconds)
        {

        }

        private void UpdateNextBuildingCost()
        {
            var ownedOverCostFactor = Math.Pow(CostFactor, Owned);
            var ownedPlusOneOverCostFactor = Math.Pow(CostFactor, Owned + 1);

            var NextBuildingCostForOne = BaseCost * ((ownedOverCostFactor - ownedPlusOneOverCostFactor) / (1 - CostFactor));
        }
    }
}
