﻿using Assets.IdleEngine.Generator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.IdleEngine.Sessions
{
    [CreateAssetMenu(fileName ="Session", menuName ="Game/Session", order = 0)]
    public class Session : ScriptableObject
    {
        public IdleGenerator[] Generators;
        public double Points = 0;
    }
}
